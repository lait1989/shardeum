#!/bin/bash
set -e
_PASS=$1
_LPORT=$2
_EIP=$3
_IIP=$4
_FP2PPORT=$5
_SP2PPORT=$6

echo "${_PASS}-${_LPORT}-${_EIP}-${_IIP}-${_FP2PPORT}-${_SP2PPORT}"

apt-get install curl
apt-get update
apt install docker.io
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
service docker restart
sudo -u "$USER" curl -O https://gitlab.com/shardeum/validator/dashboard/-/raw/main/installer.sh && chmod +x installer.shsudo -u "$USER" echo -e "Y\nY\n${_PASS}\n${_LPORT}\n${_EIP}\n${_IIP}\n\n${_FP2PPORT}\n\n${_SP2PPORT}\n" | ./installer.sh
