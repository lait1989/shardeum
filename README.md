# Shardeum


## Getting started

The script has some options that you can give it


- _PASS=$1 (password) required
- _LPORT=$2 (localhost port) required | default 8080
- _EIP=$3 (external ip) if you don't need this option set it to auto
- _IIP=$4 (internal ip) if you don't need this option set it to auto
- _FP2PPORT=$5 (first p2p port) default 9001 recommend set it (1025-65536)
- _SP2PPORT=$6 (second p2p port) default 10001 recommend set it (1025-65536)

## Run script

sudo ./shardeum.sh pass 10800 auto auto 9002 10002
